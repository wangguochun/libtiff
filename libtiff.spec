Name:           libtiff
Version:        4.6.0
Release:        3
Summary:        TIFF Library and Utilities
License:        libtiff
URL:            https://libtiff.gitlab.io/libtiff/
Source0:        https://download.osgeo.org/libtiff/tiff-%{version}.tar.gz

Patch6000:      backport-CVE-2023-6228.patch
Patch6001:      backport-0001-CVE-2023-6277.patch
Patch6002:      backport-0002-CVE-2023-6277.patch
Patch6003:      backport-0003-CVE-2023-6277.patch
Patch6004:      backport-0004-CVE-2024-7006.patch

BuildRequires:  gcc gcc-c++ zlib-devel libjpeg-devel jbigkit-devel
BuildRequires:  libtool automake autoconf pkgconfig

%description
This %{name} provides support for the Tag Image File Format (TIFF), a widely
used format for storing image data. The latest version of the TIFF specification
is available on-line in several different formats.And contains command-line programs
for manipulating TIFF format image files using the libtiff library.

%package	devel
Summary:        Development files for %{name} library
Requires:	%{name} = %{version}-%{release} pkgconfig

%description devel
This package contains the header files and documentation necessary for developing programs
which will manipulate TIFF format image files using the libtiff library.

%package static
Summary:	Static TIFF image format file library
Requires:	%{name}-devel%{?_isa} = %{version}-%{release}

%description static
the libtiff-static package contains the syatically linkable version of libtiff.
Linking to static libraries is discouraged for most applications,but it is necessary for some boot packages.

%package tools
Summary:	Command-line utility programs for manipulating TIFF files
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description tools
This package contains command-line programs for manipulating TIFF format image files using the libtiff library.

%package_help

%prep
%autosetup -n tiff-%{version} -p1

libtoolize --force  --copy
aclocal -I . -I m4
automake --add-missing --copy
autoconf
autoheader

%build
export CFLAGS="%{optflags} -fno-strict-aliasing"
%configure --enable-ld-version-script
%make_build

%install
%make_install
%delete_la

rm -rf %{buildroot}/%{_datadir}/doc/
rm -f  %{buildroot}/%{_bindir}/tiffgt

case `uname -i` in
    x86_64 )
         wordsize="64"
    ;;
    *)
         wordsize=""
    ;;
esac

if test -n "$wordsize"
then
   mv %{buildroot}/%{_includedir}/tiffconf.h  %{buildroot}/%{_includedir}/tiffconf-$wordsize.h
   cat >%{buildroot}/%{_includedir}/tiffconf.h <<EOF
#ifndef TIFFCONF_H_MULTILIB
#define TIFFCONF_H_MULTILIB

#include <bits/wordsize.h>

#if __WORDSIZE == 32
# include "tiffconf-32.h"
#elif __WORDSIZE == 64
# include "tiffconf-64.h"
#else
# error "unexpected value for __WORDSIZE macro"
#endif

#endif
EOF
fi

%ldconfig_scriptlets

%check
make check
find doc -name 'Makefile*' | xargs rm

%files
%defattr(-,root,root)
%license LICENSE.md
%doc README.md
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/

%files static
%{_libdir}/*.a

%files tools
%{_bindir}/*
%{_mandir}/man1/*

%files help
%defattr(-,root,root)
%{_mandir}/man*
%doc RELEASE-DATE VERSION
%doc TODO ChangeLog doc
%exclude %{_mandir}/man1/*

%changelog
* Tue Aug 13 2024 wangguochun <wangguochun@kylinos.cn> - 4.6.0-3
- fix CVE-2024-7006

* Mon Jul 22 2024 xuguangmin <xuguangmin@kylinos.cn> - 4.6.0-2
- Fix incorrect dates in the ChangeLog section of the spec file.

* Wed Dec 27 2023 lvgenggeng <lvgenggeng@uniontech.com> - 4.6.0-1
- bump to 4.6.0

* Wed Nov 29 2023 liningjie <liningjie@xfusion.com> - 4.5.1-4
- backport patch for fix CVE-2023-6277 issue

* Sat Nov 25 2023 liningjie <liningjie@xfusion.com> - 4.5.1-3
- fix CVE-2023-6277

* Tue Nov 21 2023 liningjie <liningjie@xfusion.com> - 4.5.1-2
- fix CVE-2023-6228

* Mon Jul 24 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 4.5.1-1
- update 4.5.1

* Thu Jul 13 2023 zhangpan <zhangpan103@h-partners.com> - 4.5.0-8
- fix CVE-2023-3576

* Tue Jul 04 2023 zhangpan <zhangpan103@h-partners.com> - 4.5.0-7
- fix CVE-2023-25433 CVE-2023-26966 CVE-2023-2908

* Sun Jun 25 2023 zhangpan <zhangpan103@h-partners.com> - 4.5.0-6
- fix CVE-2023-3316

* Thu Jun 15 2023 zhangpan <zhangpan103@h-partners.com> - 4.5.0-5
- fix CVE-2023-26965

* Wed May 24 2023 zhangpan <zhangpan103@h-partners.com> - 4.5.0-4
- fix CVE-2023-2731

* Mon Feb 20 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 4.5.0-3
- delete old so files

* Thu Feb 16 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 4.5.0-2
- fix CVE-2023-0795,CVE-2023-0796,CVE-2023-0797,CVE-2023-0798,CVE-2023-0799,
- fix CVE-2023-0800,CVE-2023-0801,CVE-2023-0802,CVE-2023-0803,CVE-2023-0804

* Thu Feb 09 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 4.5.0-1
- update to 4.5.0

* Sun Jan 29 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 4.3.0-22
- Type:cve
- ID:CVE-2022-48281
- SUG:NA
- DESC:fix CVE-2022-48281

* Thu Nov 17 2022 qisen <qisen@huawei.com> - 4.3.0-21
- Type:cve
- ID:CVE-2022-3970
- SUG:NA
- DESC:fix CVE-2022-3970

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 4.3.0-20
- fix CVE-2022-3570,CVE-2022-3597,CVE-2022-3598,CVE-2022-3599,CVE-2022-3626,CVE-2022-3627

* Mon Oct 17 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 4.3.0-19
- fix CVE-2022-2056,CVE-2022-2057,CVE-2022-2058

* Tue Sep 13 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 4.3.0-18
- fix CVE-2022-2953,CVE-2022-2519,CVE-2022-2520,CVE-2022-2521

* Tue Aug 23 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 4.3.0-17
- fix CVE-2022-2867,CVE-2022-2868,CVE-2022-2869

* Tue Jul 05 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 4.3.0-16
- fix CVE-2022-1354

* Tue Jun 14 2022 wuchaochao <cyanrose@yeah.net> - 4.3.0-15
- fix CVE-2022-1622 and CVE-2022-1623

* Sat Jun 11 2022 wangkerong <wangkerong@h-partners.com> - 4.3.0-14
- delete macro in changelog

* Wed May 18 2022 liuyumeng <liuyumeng5@h-partners.com> - 4.3.0-13
- fix CVE-2022-1355

* Fri Apr 01 2022 dongyuzhen <dongyuzhen@h-partners.com> - 4.3.0-12
- fix CVE-2022-0909,CVE-2022-0924

* Tue Mar 29 2022 yangcheng <yangcheng87@h-partners.com> - 4.3.0-11
- fix CVE-2022-0865

* Mon Mar 28 2022 yangcheng <yangcheng87@h-partners.com> - 4.3.0-10
- fix CVE-2022-0907

* Tue Mar 22 2022 yangcheng <yangcheng87@h-partners.com> - 4.3.0-9
- Type:cve
- ID:CVE-2022-0908
- SUG:NA
- DESC:fix CVE-2022-0908

* Thu Mar 17 2022 wangkerong <wangkerong@h-partners.com> - 4.3.0-8
- Type:cve
- ID:CVE-2022-0891
- SUG:NA
- DESC:fix CVE-2022-0891

* Tue Mar 08 2022 dongyuzhen <dongyuzhen@h-partners.com> - 4.3.0-7
- Type:cves
- ID:CVE-2022-22844 
- SUG:NA
- DESC:fix CVE-2022-22844 

* Wed Feb 23 2022 liuyumeng <liuyumeng5@h-partners.com> - 4.3.0-6
- Type:cves
- ID:CVE-2022-0561 CVE-2022-0562
- SUG:NA
- DESC:fix CVE-2022-0561 CVE-2022-0562

* Fri Dec 24 2021 liuyumeng <liuyumeng5@huawei.com> - 4.3.0-5
- fix the dependency package connot find TIFF_SSIZE_T in tiffio.h

* Wed Dec 15 2021 liuyumeng <liuyumeng5@huawei.com> - 4.3.0-4
- fix raw2tiff floating point exception

* Mon Dec 13 2021 liuyumeng <liuyumeng5@huawei.com> - 4.3.0-3
- fix incorrect writing when unpacking in spec

* Mon Dec 06 2021 liuyumeng <liuyumeng5@huawei.com> - 4.3.0-2 
- fix the date in the changelog

* Fri Dec 03 2021 liuyumeng <liuyumeng5@huawei.com> - 4.3.0-1
- update to libtiff-4.3.0-1

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 4.2.0-2
- DESC: delete -S git from autosetup, and delete BuildRequires git

* Wed Jan 27 2021 hanhui <hanhui15@huawei.com> - 4.2.0-1
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: update to 4.2.0

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.1.0-1
- update to 4.1.0

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.0.10-2
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify the location of COPYRIGHT

* Fri Sep 06 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.0.10-1
- Type:Enhance
- ID:NA
- SUG:NA
- DESC: openEuler Debranding

* Mon Aug 19 2019 cangyi<cangyi@huawei.com> - 4.0.9-11.h6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:format patches

* Fri Aug 09 2019 zhangyujing<zhangyujing1@huawei.com> - 4.0.9-11.h5
- Type:cves
- ID:CVE-2018-10779
- SUG:NA
- DESC:fix CVE-2018-10779

* Wed Jul 31 2019 shenyangyang<shenyangyang4@huawei.com> - 4.0.9-11.h4
- Type:NA
- ID:NA
- SUG:NA
- DESC:openEuler Debrading

* Mon Jul 15 2019 wangchan<wangchan9@huawei.com> - 4.0.9-11.h3
- Type:cves
- ID:CVE-2017-17095
- SUG:NA
- DESC:fix CVE-2017-17095

* Mon Apr 29 2019 yuejiayan<yuejiayan@huawei.com> - 4.0.9-11.h2
- Type:cves
- ID:CVE-2018-19210 CVE-2019-6128
- SUG:NA
- DESC:fix above cves

* Sun Apr 07 2019 wenjun<wenjun8@huawei.com> - 4.0.9-11.h1
- Type:cves
- ID:CVE-2018-18557 CVE-2018-17101 CVE-2018-17100 CVE-2018-12900 CVE-2018-18661
- SUG:NA
- DESC:fix above cves

* Fri Jul 13 2018 shenyangyang<shenyangyang4@huawei.com> - 4.0.9-11
- Package Initialization

